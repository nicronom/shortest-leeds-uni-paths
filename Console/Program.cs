﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using Source;
using System.IO;

namespace ConsoleApp
{
    public class Program
    {
        private readonly List<string> _nodes;
        private readonly GraphData _graph;
        private readonly PathFinder _paths;

        public Program(string fileName)
        {
            _nodes = new GraphParser(GetText(fileName)).GetNodes();
            _graph = new GraphParser(GetText(fileName)).Parse();
            _paths = new PathFinder(_nodes, _graph.Edges);
        }

        private static string[] GetText(string fileName)
        {
            return File.ReadAllLines(fileName) ?? throw new ArgumentNullException(nameof(fileName));
        }

        public void ProgramInit()
        {
            HandleMenu();
            Console.WriteLine("Press key to terminate");
            Console.ReadKey();
        }
        private bool GetAnswer(string question)
        {
            Console.WriteLine(question);

            bool answer = false;
            bool invalidInput = false;
            do
            {
                Console.WriteLine("[Y]es or [N]o?");
                char input = Console.ReadKey().KeyChar;

                if (input == 'Y' || input == 'y')
                {
                    answer = true;
                    invalidInput = false;
                }
                else if (input == 'N' || input == 'n')
                {
                    invalidInput = false;
                }
                else
                {
                    Console.WriteLine("Invalid input. Try again");
                    Console.WriteLine();
                    invalidInput = true;
                }
            } while (invalidInput);

            Console.WriteLine();
            return answer;
        }

        private void DisplayMenu()
        {
            Console.WriteLine("-------MENU-------");
            Console.WriteLine("1) List of Nodes");
            Console.WriteLine("2) List of Edges");
            Console.WriteLine("3) Find path");
            Console.WriteLine("4) Test on artbitrary nodes");
            Console.WriteLine("5) Exit");
            Console.WriteLine();
            Console.WriteLine("Enter your choice: ");
        }

        private void HandleMenu()
        {
            bool validInput;

            while (true)
            {
                do
                {
                    DisplayMenu();
                    char input = Console.ReadKey().KeyChar;
                    validInput = true;
                    switch (input)
                    {
                        case '1':
                            DisplayNodes();
                            break;
                        case '2':
                            DisplayEdges();
                            break;
                        case '3':
                            FindPath();
                            break;
                        case '4':
                            ArbitraryNodesPath();
                            break;
                        case '5':
                            Environment.Exit(0);
                            break;
                        default:
                            validInput = false;
                            Console.WriteLine("Invalid input. Try again");
                            Console.WriteLine();
                            break;

                    }
                } while (validInput);
            }
        }

        private void DisplayNodes()
        {
            foreach(var node in _graph.Nodes)
            {
                if(node.Value.Length != 2)
                {
                    throw new ArgumentException("Invalid node");
                }

                Console.WriteLine("Node: {0} Lat: {1} Lng: {2}", node.Key, node.Value[0], node.Value[1]);
                Console.WriteLine();
            }
        }

        private void DisplayEdges()
        {
            foreach(var edge in _graph.Edges)
            {
                if(edge.Nodes.Length != 2)
                {
                    throw new ArgumentException("Invalid edge");
                }

                Console.WriteLine("Edge: {0} between Nodes {1} {2}", edge.Id, edge.Nodes[0], edge.Nodes[1]);
                Console.WriteLine("Edge Length: {0}", edge.Length);
                Console.WriteLine();
            }
        }

        private void FindPath()
        {
            string sourceNode, destinationNode;

            Console.WriteLine("Please enter source node id: ");
            sourceNode = Console.ReadLine();
            Console.WriteLine("Please enter destination node id: ");
            destinationNode = Console.ReadLine();
            Console.WriteLine("Assimilating node existence");
            Console.WriteLine("##########.................");
            Console.WriteLine();

            bool sourceNodeFound = false, destinationNodeFound = false;
            foreach (var node in _graph.Nodes)
            {
                if(node.Key == sourceNode)
                {
                    sourceNodeFound = true;
                }

                else if(node.Key == destinationNode)
                {
                    destinationNodeFound = true;
                }

                else if(sourceNodeFound && destinationNodeFound)
                {
                    break;
                }
            }

            if(sourceNodeFound == false && destinationNodeFound == false)
            {
                Console.WriteLine("Sorry you have entered an invalid node");
                Console.WriteLine();
                return;      
            }

            PrintPath(sourceNode, destinationNode);
            return;
        }

        private void ArbitraryNodesPath()
        {
            Random rng = new Random();
            int sourceNode = rng.Next(0, _graph.Nodes.Keys.Count);
            int destinationNode = rng.Next(0, _graph.Nodes.Keys.Count);

            Console.WriteLine("Attempts to find path between:");
            Console.WriteLine("Source: {0} Destination {1}",
                _nodes[sourceNode], _nodes[destinationNode]);
            PrintPath(_nodes[sourceNode], _nodes[destinationNode]);
        }

        private void PrintPath(string sourceNode, string destinationNode)
        {
            Console.WriteLine("Calculating shortest path");
            Console.WriteLine("############.............");
            List<string> thePath = _paths.GetShortestPath(sourceNode, destinationNode);
            if (thePath == null)
            {
                Console.WriteLine("There is no path from {0} to {1}", sourceNode, destinationNode);
                Console.WriteLine();
                return;
            }

            StringBuilder sb = new StringBuilder();
            int atNode = 0;
            foreach (string node in thePath)
            {
                sb.Append(node);
                if (atNode != thePath.Count - 1)
                {
                    sb.Append("-->");
                }
                atNode++;
            }

            Console.WriteLine("Shortest path from {0} to {1} is: ", sourceNode, destinationNode);
            Console.WriteLine(sb.ToString());
            Console.WriteLine("Path Length: {0}", _paths.PathLength());
            Console.WriteLine();
            return;
        }
    }
}
