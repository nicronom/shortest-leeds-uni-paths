﻿using System;
using System.Collections.Generic;

namespace Source
{
    public class ParseFormatter
    {
        private readonly string _text;
        private readonly string _type;

        public ParseFormatter(string text, string type)
        {
            _text = text ?? throw new ArgumentNullException(nameof(text));
            _type = type ?? throw new ArgumentNullException(nameof(type));
        }

        public List<KeyValuePair<string, string>> GetAttributes()
        {
            string[] attributes = StripText().Split(' ');
            List<KeyValuePair<string, string>> attributesList = new List<KeyValuePair<string, string>>();

            foreach (string attribute in attributes)
            {
                if (String.IsNullOrEmpty(attribute))
                    continue;

                KeyValuePair<string, string> linkAttribute = ParseAttribute(attribute);
                attributesList.Add(linkAttribute);
            }

            return attributesList;
        }

        private static KeyValuePair<string, string> ParseAttribute(string attribute)
        {
            string[] attributes = attribute.Split('=');

            if (attributes.Length != 2)
                throw new ArgumentException($"Invalid number of arguments in {attribute}");

            return new KeyValuePair<string, string>(
                key: attributes[0],
                value: attributes[1]);
        }

        private string StripText()
        {
            string Beginning = $"<{_type} ";
            string Ending;

            if (_type == "link")
                Ending = $";/{_type}>";

            else
                Ending = $" /{_type}>";

            string withoutBeginning = _text.Substring(Beginning.Length);

            return
                withoutBeginning.Substring(0, withoutBeginning.Length - Ending.Length);
        }
      }
    }
