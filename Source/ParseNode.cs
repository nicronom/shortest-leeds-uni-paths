﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Source
{
    public class ParseNode
    {
        private readonly string _text;
        private const string _elementText = "node";
        private readonly ParseFormatter _parser;

        public ParseNode(string text)
        {
            _text = text ?? throw new ArgumentNullException(nameof(text));
            _parser = new ParseFormatter(text, _elementText);
        }

        public KeyValuePair<string, double[]> GetNode()
        {
            if (!_text.Contains(@"/node"))
                throw new ArgumentException("Line does not contain attribute /node");

            return FormatAttributes();
        }

        private KeyValuePair<string,double[]> FormatAttributes()
        {
            List<KeyValuePair<string, string>> nodeAttributes = _parser.GetAttributes();
            ValidateAttributes(nodeAttributes);

            double[] parameters = new double[2];
            parameters[0] = double.Parse(nodeAttributes.Single(attribute => attribute.Key == "lat").Value);
            parameters[1] = double.Parse(nodeAttributes.Single(attribute => attribute.Key == "lon").Value);
            string nodeId = nodeAttributes.Single(attribute => attribute.Key == "id").Value;


            return new KeyValuePair<string, double[]>(nodeId, parameters);
            
        }

        private static void ValidateAttributes(List<KeyValuePair<string, string>> attributes)
        {
            List<string> attributeKeys = attributes.Select(attribute => attribute.Key).ToList();
            if (attributeKeys.Count(key => key == "id") > 1)
                throw new ArgumentException("Invalid number of id attributes");

            if (attributeKeys.Count(key => key == "lat") > 1)
                throw new ArgumentException("Invalid number of latitude attributes");

            if (attributeKeys.Count(key => key == "longitude") > 1)
                throw new ArgumentException("Invalid number of longitude attributes");

        }
    }
}