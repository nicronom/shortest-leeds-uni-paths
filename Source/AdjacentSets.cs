﻿using System;
using System.Collections.Generic;
using Models;

namespace Source
{
    //Class to find all adjacencies in a given Graph
    //with nodes and edges
    public class AdjacentSets
    {
        private readonly List<string> _nodes;
        private readonly List<Edge> _edges;
        private Dictionary<string, List<KeyValuePair<string, double>>> _adjNodes =
            new Dictionary<string, List<KeyValuePair<string, double>>>();

        public AdjacentSets(List<string> nodes, List<Edge> edges)
        {
            _nodes = nodes ?? throw new ArgumentNullException("Null node array passed in constructor");
            _edges = edges ?? throw new ArgumentNullException("Null node array passed in constructor");
        }

        //@returns a dictionary of unique nodes with values
        //their neighbouring vertices and the length of each 
        //path between the node and the given neighbouring vertex
        public Dictionary<string, List<KeyValuePair<string,double>>> GetAll()
        {
            foreach(string node in _nodes)
            {
                if (node == null) throw new ArgumentNullException("Null node at GetAll");

                _adjNodes.Add(node, GetAdjacents(node));
            }

            if(_adjNodes.Count == 0)
            {
                return null;
            }

            return _adjNodes;
        }

        public List<KeyValuePair<string,double>> GetSingle(string node)
        {
            return GetAdjacents(node);
        }

        private List<KeyValuePair<string,double>> GetAdjacents(string node)
        {
            if(node == null) throw new ArgumentNullException("Null node");

            List<KeyValuePair<string, double>> neighbours = new List<KeyValuePair<string, double>>();
            
            foreach(Edge edge in _edges)
            {
                if (edge.Nodes.Length != 2) throw new ArgumentException("Any edge should contain exactly 2 nodes");

                int neighbourIndex = 0;

                if (node == edge.Nodes[0] || node == edge.Nodes[1])
                {
                    if (node == edge.Nodes[0])
                    {
                        neighbourIndex = 1; //to know which node to store as neighbour
                    }

                    neighbours.Add(new KeyValuePair<string, double>
                        (edge.Nodes[neighbourIndex], edge.Length));
                }
            }

            if (neighbours.Count == 0) return null;

            return neighbours;
        }
    }
}
