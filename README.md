# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This project represents an example solution for 
* first year computer science programming project
* @title Route Finding
* 
* The console application can find the shortest path
* between any arbitrary nodes present in the graph
* representation of University of Leeds ( if path exists )
*
* Version 1.0.0

### How do I get set up? ###

* Summary of set up
*******************
* Open project solution in VS

* How to run tests
*******************
* Select Run All Tests in VS

### Who do I talk to? ###

* project author and repository admin