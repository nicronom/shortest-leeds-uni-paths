﻿namespace Models
{
    public class Edge
    {
        public string Id { get; set; }
        public string[] Nodes { get; set; }
        public double Length { get; set; }
    }
}
